# controls #

v1.1.4

This module defines generic controllers for desktop and mobile devices.
This module also prevents the images from being dragged on desktop view.

## Attributes ##

** Controls **

| Attribute 		| Description 							|
| ------------------| ------------------------------------- |
| click				| Click 								|
| pressed 			| Beginning of a click 					|
| released 			| End of a click 						|
| cursorEnter 		| The cursor enters the block 			|
| cursorLeave 		| The cursor leaves the block 			|
| cursorMove 		| The cursor is moving in the block 	|
| cursorDragStart 	| The block is beginning to be dragged	|
| cursorDragEnd		| The block is no longer dragged 		|

** Flags **

| Attribute 		| Description 							|
| ------------------| ------------------------------------- |
| isPressed			| true if there is a click going on 	|
| isScrolling 		| true if there is a scroll going on 	|

## Usage ##

```js
var div = document.getElementById('my-div');
div.addEventListener(Controls.click, function(){alert('Hello World');}, false);
```