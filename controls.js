define(function(){
	var Controls = {};

	// --- CONTROLS ---
		if(window.cordova || /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
			Controls.click 				= 'click';
			Controls.pressed 			= 'touchstart';
			Controls.released 			= 'touchend';
			Controls.cursorEnter 		= 'touchenter';
			Controls.cursorLeave 		= 'touchleave';
			Controls.cursorDragStart 	= 'touchmove';
			Controls.cursorDragEnd 		= 'dragend';
			Controls.cursorMove 		= 'touchmove';
		}else{
			Controls.click 				= 'click';
			Controls.pressed 			= 'mousedown';
			Controls.released 			= 'mouseup';
			Controls.cursorEnter 		= 'mouseenter';
			Controls.cursorLeave 		= 'mouseleave';
			Controls.cursorDragStart 	= 'dragstart';
			Controls.cursorDragEnd 		= 'dragend'
			Controls.cursorMove 		= 'mousemove';

			document.addEventListener('dragstart', function(e){e.preventDefault();}, false);	// PREVENT DRAGGING
		}

		// --- FLAGS ---
			Controls.isPressed 		= false;
			Controls.isScrolling 	= false;

			document.addEventListener('scroll',				function(){Controls.isScrolling = true;}, true);									// Flag Scroll
			document.addEventListener(Controls.pressed, 	function(){Controls.isPressed 	= true;}, false);									// Flag mousedown
			document.addEventListener(Controls.released, 	function(){Controls.isPressed 	= false; Controls.isScrolling = false;}, false);	// Flag mouseup, øScroll

	return Controls;
});